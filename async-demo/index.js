console.log('Before');

// Promise-based approach
// getUser(1)
//     .then(user => getRepositories(user.githubUsername))
//     .then(repositories => getCommits(repositories[0]))
//     .then(commits => console.log('Commits', commits))
//     .catch(error => console.error(error.message));

// Async and Await approach
async function displayCommits() {
    try {
        const user = await getUser(1);
        const repositories = await getRepositories(user.githubUsername);
        const commits = await getCommits(repositories[0]);
        console.log(commits);
    } catch (error) {
        console.error(error);
    }
}
displayCommits();

console.log('After');



function getUser(id) {
    return new Promise((resolve, reject) => {
        // Kick iff some async work
        setTimeout(() => {
            console.log('Reading a user from a database...');
            resolve({ id: id, githubUsername: 'tobiaswaelde' });
        }, 1000);
    });
}

function getRepositories(username) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log(`Reading repositories for user '${username}'...`);
            // resolve(['repo1', 'repo2', 'repo3']);
            reject(new Error('Could not get the repositories...'));
        }, 1000);
    });
}

function getCommits(repository) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('Reading commits from repository...');
            resolve(['commit']);
        }, 1000);
    });
}
