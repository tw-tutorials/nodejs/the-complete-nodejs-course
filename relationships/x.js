
// Trade off between query performance vs consistency


// Using references (Normalization) -> CONSISTENCY
let author = {
    name: 'Mosh'
};

let course = {
    author: 'id'
    // authors: [

    // ]
};


// Using embedded documents (Denormalization) -> PERFORMANCE
let course = {
    author: {
        name: 'Mosh'
    }
};


// Hybrid
let author = {
    name: 'Mosh'
    // ...
};

let course = {
    author: {
        id: 'ref',
        name: 'Mosh'
    }
}