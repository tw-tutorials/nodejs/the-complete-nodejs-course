const jwt = require('jsonwebtoken');
const config = require('config');

function auth(request, response, next) {
    const token = request.header('x-auth-token');
    if (!token) return response.status(401).send('Access denied. No token provided.');

    try {
        const payload = jwt.verify(token, config.get('jwtPrivateKey'));
        request.user = payload;

        next();
    } catch (ex) {
        return response.status(400).send('Invalid token.');
    }
}

module.exports = auth;