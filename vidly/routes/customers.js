const express = require('express');

const auth = require('../middleware/auth/auth');
const { Customer, validate } = require('../models/customer');

const router = express.Router();


router.get('/', async (request, response) => {
    const customers = await Customer
        .find()
        .sort({ name: 1 });
    response.send(customers);
});

router.get('/:id', async (request, response) => {
    const customer = await Customer.findById(request.params.id);

    if (!customer) {
        return response
            .status(404)
            .send('The customer with the given ID was not found.');
    }

    response.send(customer);
});

router.post('/', auth, async (request, response) => {
    const { error } = validate(request.body);
    if (error) {
        return response
            .status(400)
            .send(error.details[0].message);
    }

    const customer = new Customer({
        name: request.body.name,
        phone: request.body.phone,
        isGold: request.body.isGold
    });
    await customer.save();

    response.send(customer);
});

router.put('/:id', auth, async (request, response) => {
    const { error } = validate(request.body);
    if (error) {
        return response
            .status(400)
            .send(error.details[0].message);
    }

    const customer = await Customer.findByIdAndUpdate(
        request.params.id,
        {
            name: request.body.name,
            phone: request.body.phone,
            isGold: request.body.isGold
        },
        { new: true }
    );

    if (!customer) {
        return response
            .status(404)
            .send('The customer with the given ID was not found.');
    }

    response.send(customer);
});

router.delete('/:id', auth, async (request, response) => {
    const customer = await Customer.findByIdAndRemove(request.params.id);

    if (!customer) {
        return response
            .status(404)
            .send('The customer with the given ID was not found.');
    }

    response.send(customer);
});


module.exports = router;