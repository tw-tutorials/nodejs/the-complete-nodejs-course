const express = require('express');

const auth = require('../middleware/auth/auth');
const { Movie, validate } = require('../models/movie');
const { Genre } = require('../models/genre');

const router = express.Router();


router.get('/', async (reqest, response) => {
    const movies = await Movie.find()
        .sort({ title: 1 });

    response.send(movies);
});

router.get('/:id', async (request, response) => {
    const movie = await Movie.findById(request.params.id);

    if (!movie) {
        return response
            .status(404)
            .send('The movie with the given ID was not found.');
    }

    response.send(movie);
});

router.post('/', auth, async (request, response) => {
    const { error } = validate(request.body);
    if (error) {
        return response
            .status(400)
            .send(error.details[0].message);
    }

    const genre = await Genre.findById(request.body.genreId);
    if (!genre) {
        return response
            .status(400)
            .send('Invalid genre.');
    }

    const movie = new Movie({
        title: request.body.title,
        genre: {
            _id: genre.id,
            name: genre.name
        },
        numberInStock: request.body.numberInStock,
        dailyRentalRate: request.body.dailyRentalRate
    });
    await movie.save();

    response.send(movie);
});

router.put('/:id', auth, async (request, response) => {
    const { error } = validate(request.body);
    if (error) {
        return response
            .status(400)
            .send(error.details[0].message);
    }

    const genre = await Genre.findById(request.body.genreId);
    if (!genre) {
        return response
            .status(400)
            .send('Invalid genre.');
    }

    const movie = await Movie.findByIdAndUpdate(
        request.params.id,
        {
            title: request.body.title,
            genre: {
                _id: genre.id,
                name: genre.name
            },
            numberInStock: request.body.numberInStock,
            dailyRentalRate: request.body.dailyRentalRate
        },
        { new: true }
    );

    if (!movie) {
        return response
            .status(404)
            .send('The movie with the given ID was not found.');
    }

    response.send(movie);
});

router.delete('/:id', auth, async (request, response) => {
    const movie = await Movie.findByIdAndRemove(request.params.id);

    if (!movie) {
        return response
            .status(404)
            .send('The movie with the given ID was not found.');
    }

    response.send(movie);
});


module.exports = router;