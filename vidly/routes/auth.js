const express = require('express');
const Joi = require('joi');
const _ = require('lodash');
const bcrypt = require('bcrypt');

const { User } = require('../models/user');

const router = express.Router();


router.post('/', async (request, response) => {
    const { error } = validate(request.body);
    if (error) return response.status(400).send(error.details[0].message);

    // Check if user is registered
    let user = await User.findOne({ email: request.body.email });
    if (!user) return response.status(400).send('Invalid email or password.');

    // Check if password is correct
    const validPassword = await bcrypt.compare(request.body.password, user.password);
    if (!validPassword) return response.status(400).send('Invalid email or password.');


    // Generate JWT
    const token = user.generateAuthToken();

    response.send(token);
});


function validate(request) {
    const schema = {
        email: Joi.string().min(5).max(255).required().email(),
        password: Joi.string().min(5).max(255).required()
    };

    return Joi.validate(request, schema);
}


module.exports = router;