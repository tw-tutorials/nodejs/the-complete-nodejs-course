const express = require('express');
const _ = require('lodash');
const bcrypt = require('bcrypt');

const auth = require('../middleware/auth/auth');
const { User, validate } = require('../models/user');

const router = express.Router();


router.get('/me', auth, async (request, response) => {
    const id = request.user._id;
    const user = await User.findById(id)
        .select({ password: 0 });

    response.send(user);
});

router.post('/', async (request, response) => {
    const { error } = validate(request.body);
    if (error) return response.status(400).send(error.details[0].message);

    let user = await User.findOne({ email: request.body.email });
    if (user) return response.status(400).send('User already registered.');

    user = new User(_.pick(request.body, ['name', 'email', 'password']));
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);

    await user.save();

    const token = user.generateAuthToken();

    response.header('x-auth-token', token) // Header sent by the application should get prefixed with 'x-'
        .send(_.pick(user, ['_id', 'name', 'email']));
});


module.exports = router;